import { ConfigData, FileSystemConfigLoader, HtmlValidate, Message, Report } from "html-validate";
import Plugin from "../src";

const config: ConfigData = {
	root: true,
	plugins: [Plugin],
	extends: ["html-validate:recommended"],
	transform: {
		"\\.js$": "html-validate-angular:js",
		"\\.html$": "html-validate-angular:html",
	},
};
const loader = new FileSystemConfigLoader(config);

/**
 * Filter out properties not present in all supported versions of html-validate (see
 * peerDependencies). This required in the version matrix integration test.
 */
function filterReport(report: Report): void {
	for (const result of report.results) {
		for (const msg of result.messages) {
			const src: Partial<Message & { ruleUrl: string }> = msg;
			delete src.ruleUrl;
			delete src.selector;
			delete src.context;
		}
	}
}

it('should find errors in "component.js"', async () => {
	expect.assertions(4);
	const htmlvalidate = new HtmlValidate(loader);
	const report = await htmlvalidate.validateFile("test/component.js");
	filterReport(report);
	expect(report.valid).toBeFalsy();
	expect(report.results[0].errorCount).toBe(1);
	expect(report.results[0].warningCount).toBe(0);
	expect(report.results[0].messages).toEqual([
		expect.objectContaining({
			ruleId: "close-order",
		}),
	]);
});

it('should find errors in "route.js"', async () => {
	expect.assertions(4);
	const htmlvalidate = new HtmlValidate(loader);
	const report = await htmlvalidate.validateFile("test/route.js");
	filterReport(report);
	expect(report.valid).toBeFalsy();
	expect(report.results[0].errorCount).toBe(2);
	expect(report.results[0].warningCount).toBe(0);
	expect(report.results[0].messages).toEqual([
		expect.objectContaining({
			ruleId: "close-order",
		}),
		expect.objectContaining({
			ruleId: expect.stringMatching(/^(no-implicit-button-type|element-required-attributes)$/),
		}),
	]);
});

it('should find errors in "route-params.js"', async () => {
	expect.assertions(4);
	const htmlvalidate = new HtmlValidate(loader);
	const report = await htmlvalidate.validateFile("test/route-params.js");
	filterReport(report);
	expect(report.valid).toBeFalsy();
	expect(report.results[0].errorCount).toBe(2);
	expect(report.results[0].warningCount).toBe(0);
	expect(report.results[0].messages).toEqual([
		expect.objectContaining({
			ruleId: "close-order",
			line: 4,
		}),
		expect.objectContaining({
			ruleId: "close-order",
			line: 12,
		}),
	]);
});

it('should find errors in "template.html"', async () => {
	expect.assertions(2);
	const htmlvalidate = new HtmlValidate(loader);
	const report = await htmlvalidate.validateFile("test/template.html");
	filterReport(report);
	expect(report.valid).toBeFalsy();
	expect(report.results).toMatchSnapshot([
		{
			messages: [
				{
					message: expect.stringMatching(
						/<button> is missing (required|recommended) "type" attribute/,
					),
				},
				{},
				{},
				{},
			],
		},
	]);
});

it('"regressions.html"', async () => {
	expect.assertions(2);
	const loader = new FileSystemConfigLoader({
		...config,
		rules: {
			"void-style": ["error", { style: "selfclose" }],
		},
	});
	const htmlvalidate = new HtmlValidate(loader);
	const report = await htmlvalidate.validateFile("test/regressions.html");
	filterReport(report);
	expect(report.results).toMatchSnapshot();
	expect(report.valid).toBeTruthy();
});
