export function routeConfig($routeProvider) {
	$routeProvider
		.when("/foo/arrow-1/:id", {
			template: (param) => `<my-component binding="${param.id}">`,
		})
		.when("/foo/arrow-2/:id", {
			template: (param) => {
				return `<my-component binding="${param.id}">`;
			},
		})
		.when("/foo/arrow-3", {
			template: () => "<my-component>",
		})
		.when("/foo/function/:id", {
			template(param) {
				return `<my-component binding="${param.id}">`;
			},
		});
}
