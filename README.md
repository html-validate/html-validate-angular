# html-validate-angular

[![pipeline status](https://gitlab.com/html-validate/html-validate-angular/badges/master/pipeline.svg)](https://gitlab.com/html-validate/html-validate-angular/commits/master)
[![coverage report](https://gitlab.com/html-validate/html-validate-angular/badges/master/coverage.svg)](https://gitlab.com/html-validate/html-validate-angular/commits/master)

Angular (2.x and later) and AngularJS (1.x) support for [html-validate].

- Extracts templates from components and routes with inline templates.
- Angular 2.x or later: Transforms property and attribute bindings in HTML.
- Angular 1.x: Transforms interpolated attributes (including `ng-attr-*`) in HTML.
- Handles dynamic bindings for rules checking presence of text.

Typescript is not yet supported. [Help wanted](https://gitlab.com/html-validate/html-validate/-/issues/94)

[html-validate]: https://www.npmjs.com/package/html-validate

## Usage

    npm install --save-dev html-validate-angular

In `.htmlvalidate.json`:

```json
{
  "plugins": ["html-validate-angular"],
  "transform": {
    "^.*\\.html$": "html-validate-angular:html",
    "^.*\\.js$": "html-validate-angular:js"
  }
}
```

HTML processing is optional but is needed when attribute interpolation is used.

## Example

```js
export const FooComponent = {
  template: "<button>foo</button>",
};
```

```js
export function routeConfig($routeProvider) {
  $routeProvider.when("/route", { template: "<p>foo</i>" });
}
```

In both cases it will allow html-validate to parse and detect errors in the
templates:

    component.js
      2:13  error  Button is missing type attribute  button-type

    route.js
      2:51  error  Mismatched close-tag, expected '</p>' but found '</i>'  close-order
