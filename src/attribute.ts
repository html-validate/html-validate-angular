import { type AttributeData, DynamicValue } from "html-validate";

/* angularjs 1.x directives */
const directives = [
	"checked",
	"class",
	"disabled",
	"href",
	"maxlength",
	"minlength",
	"pattern",
	"readonly",
	"required",
	"selected",
	"src",
	"style",
	"value",
];

const matchBinding = /^\[(attr\.)?([^\]]*)\]/;

/* eslint-disable-next-line security/detect-non-literal-regexp -- not under user control */
const matchNgAttr = new RegExp(`^ng-(?:attr-(.*)|(${directives.join("|")}))$`);

/**
 * Handle ng-xyz, attr-xyz and [xyz] attributes.
 *
 * It yields both the original attribute (as-is) and an aliased attribute
 * xyz. E.g. `ng-src="{{ foo }}"` yields both:
 *
 * - `ng-src="{{ foo }}"`
 * - `src="DynamicValue"`
 */
function* generateDynamicAlias(attr: AttributeData, key: string): IterableIterator<AttributeData> {
	/* passthru original attribute */
	yield attr;

	/* setup ng-attr-xyz alias */
	yield {
		...attr,
		key,
		value: new DynamicValue(attr.value as string),
		originalAttribute: attr.key,
	};
}

export function* processAttribute(attr: AttributeData): IterableIterator<AttributeData> {
	/* angular 2.x or later */
	const binding = matchBinding.exec(attr.key);
	if (binding) {
		yield* generateDynamicAlias(attr, binding[2]);
		return;
	}

	/* angularjs 1.x */
	const ngAttr = matchNgAttr.exec(attr.key);
	if (ngAttr) {
		yield* generateDynamicAlias(attr, ngAttr[1] || ngAttr[2]);
		return;
	}

	/* test if attribute contains interpolation */
	/* eslint-disable-next-line sonarjs/slow-regex -- technical debt, should refactor */
	if (typeof attr.value === "string" && /{{.*}}/.exec(attr.value)) {
		yield { ...attr, value: new DynamicValue(attr.value) };
		return;
	}

	/* passthru original attribute */
	yield attr;
}
