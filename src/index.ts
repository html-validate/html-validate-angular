import { type Plugin, compatibilityCheck } from "html-validate";
import { name, peerDependencies } from "../package.json";
import transformer from "./transform";

const range = peerDependencies["html-validate"];
compatibilityCheck(name, range);

const plugin: Plugin = {
	name,
	transformer,
};

export default plugin;
