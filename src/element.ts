import { type HtmlElement, DynamicValue } from "html-validate";

function isBound(node: HtmlElement): boolean {
	return (
		node.hasAttribute("ng-bind") ||
		node.hasAttribute("ng-bind-html") ||
		node.hasAttribute("ng-bind-template") ||
		node.hasAttribute("translate")
	);
}

function isTranscluded(node: HtmlElement): boolean {
	return node.is("ng-transclude") || node.hasAttribute("ng-transclude");
}

function isPluralize(node: HtmlElement): boolean {
	return node.is("ng-pluralize") || node.hasAttribute("ng-pluralize");
}

export function processElement(node: HtmlElement): void {
	if (isBound(node) || isTranscluded(node) || isPluralize(node)) {
		node.appendText(new DynamicValue(""), node.location);
	}
}
