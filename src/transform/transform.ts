import { type Source } from "html-validate";
import { processAttribute } from "../attribute";
import { processElement } from "../element";
import { stripTemplating } from "../utils";

/**
 * @internal
 */
export function transform(source: Source): Source {
	return {
		...source,
		data: stripTemplating(source.data),
		originalData: source.data,
		hooks: {
			processAttribute,
			processElement,
		},
	};
}
