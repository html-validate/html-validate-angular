import { angularTransformHTML } from "./transform-html";
import { angularTransformJS } from "./transform-js";

const transformer = {
	html: angularTransformHTML,
	js: angularTransformJS,
} as const;

export default transformer;
