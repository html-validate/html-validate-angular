import { transformFile, transformString } from "html-validate/test-utils";
import { angularTransformJS } from "./transform-js";

it("should extract template from components", async () => {
	expect.assertions(2);
	const result = await transformFile(angularTransformJS, "./test/component.js");
	expect(result).toHaveLength(1);
	expect(result[0]).toMatchInlineSnapshot(`
		{
		  "column": 12,
		  "data": "<div>foo",
		  "filename": "./test/component.js",
		  "hooks": {
		    "processAttribute": [Function],
		    "processElement": [Function],
		  },
		  "line": 2,
		  "offset": 42,
		  "originalData": "<div>foo",
		}
	`);
});

it("should extract templates from routes", async () => {
	expect.assertions(4);
	const result = await transformFile(angularTransformJS, "./test/route.js");
	expect(result).toHaveLength(3);
	expect(result[0]).toMatchInlineSnapshot(`
		{
		  "column": 31,
		  "data": "<div>foo",
		  "filename": "./test/route.js",
		  "hooks": {
		    "processAttribute": [Function],
		    "processElement": [Function],
		  },
		  "line": 3,
		  "offset": 93,
		  "originalData": "<div>foo",
		}
	`);
	expect(result[1]).toMatchInlineSnapshot(`
		{
		  "column": 14,
		  "data": "
						<button>
							bar
						</button>
					",
		  "filename": "./test/route.js",
		  "hooks": {
		    "processAttribute": [Function],
		    "processElement": [Function],
		  },
		  "line": 5,
		  "offset": 135,
		  "originalData": "
						<button>
							bar
						</button>
					",
		}
	`);
	expect(result[2]).toMatchInlineSnapshot(`
		{
		  "column": 14,
		  "data": "<button type="{{     }}">text</button>",
		  "filename": "./test/route.js",
		  "hooks": {
		    "processAttribute": [Function],
		    "processElement": [Function],
		  },
		  "line": 12,
		  "offset": 224,
		  "originalData": "<button type="{{valid}}">text</button>",
		}
	`);
});

it("should strip templating from js", async () => {
	expect.assertions(2);
	const result = await transformString(
		angularTransformJS,
		"const c = { template: '<p>{{ foobar }}</p>' };",
	);
	expect(result).toHaveLength(1);
	expect(result[0]).toMatchInlineSnapshot(`
		{
		  "column": 23,
		  "data": "<p>{{        }}</p>",
		  "filename": "inline",
		  "hooks": {
		    "processAttribute": [Function],
		    "processElement": [Function],
		  },
		  "line": 1,
		  "offset": 23,
		  "originalData": "<p>{{ foobar }}</p>",
		}
	`);
});
