import { type Source } from "html-validate";
import { TemplateExtractor } from "@html-validate/plugin-utils";
import { transform } from "./transform";

export function angularTransformJS(source: Source): Source[] {
	const te = TemplateExtractor.fromString(source.data, source.filename);
	const sources = te.extractObjectProperty("template");
	return sources.map(transform);
}

angularTransformJS.api = 1;
