import { Config, Parser, Source, HtmlElement, DynamicValue } from "html-validate";
import { transformFile, transformString } from "html-validate/test-utils";
import { processAttribute } from "../attribute";
import { angularTransformHTML } from "./transform-html";

it("should extract template from html file", async () => {
	expect.assertions(2);
	const result = await transformFile(angularTransformHTML, "./test/template.html");
	expect(result).toHaveLength(1);
	expect(result[0]).toMatchInlineSnapshot(`
		{
		  "column": 1,
		  "data": "<div>
			<button>text</button>
			<button type="invalid">text</button>
			<button type="{{       }}">text</button>
			<button ng-attr-type="valid">text</button>
		</div>

		<!-- should detect duplicated ng-class but not duplicate class + ng-class -->
		<p class="foo" ng-class="bar"></p>
		<p ng-class="foo" ng-class="bar"></p>

		<!-- should detect ng-bind and friends as having textual content -->
		<h1></h1>
		<h2 ng-bind="foo"></h2>
		<h3 ng-bind-html="bar"></h3>
		<h3 ng-bind-template="baz"></h3>
		<h4 translate="KEY"></h4>

		<h1 ng-transclude></h1>
		<h1><ng-transclude></ng-transclude></h1>
		<h1 ng-pluralize></h1>
		<h1><ng-pluralize></ng-pluralize></h1>

		<!-- should not detect errors inside {{     }}  -->
		<div>{{        }}</div>
		<div>{{                               }}</div>
		",
		  "filename": "./test/template.html",
		  "hooks": {
		    "processAttribute": [Function],
		    "processElement": [Function],
		  },
		  "line": 1,
		  "offset": 0,
		  "originalData": "<div>
			<button>text</button>
			<button type="invalid">text</button>
			<button type="{{ valid }}">text</button>
			<button ng-attr-type="valid">text</button>
		</div>

		<!-- should detect duplicated ng-class but not duplicate class + ng-class -->
		<p class="foo" ng-class="bar"></p>
		<p ng-class="foo" ng-class="bar"></p>

		<!-- should detect ng-bind and friends as having textual content -->
		<h1></h1>
		<h2 ng-bind="foo"></h2>
		<h3 ng-bind-html="bar"></h3>
		<h3 ng-bind-template="baz"></h3>
		<h4 translate="KEY"></h4>

		<h1 ng-transclude></h1>
		<h1><ng-transclude></ng-transclude></h1>
		<h1 ng-pluralize></h1>
		<h1><ng-pluralize></ng-pluralize></h1>

		<!-- should not detect errors inside {{ ... }}  -->
		<div>{{ "</p>" }}</div>
		<div>{{ foo && bar ? 'true' : 'false' }}</div>
		",
		}
	`);
});

it("querySelector should find class with ng-class present", async () => {
	expect.assertions(3);
	const config = Config.empty();
	const resolved = await config.resolve();
	const parser = new Parser(resolved);
	const source: Source = {
		data: '<p class="foo" ng-class="bar"></p>',
		filename: "inline",
		line: 1,
		column: 1,
		offset: 0,
		hooks: {
			processAttribute,
		},
	};
	const doc = parser.parseHtml(source);
	const node = doc.querySelector(".foo")!;
	expect(node).toBeInstanceOf(HtmlElement);
	expect(Array.from(node.classList)).toEqual(["foo"]);
	expect(node.getAttribute("class", true)).toEqual([
		expect.objectContaining({
			key: "class",
			value: "foo",
		}),
		expect.objectContaining({
			key: "class",
			value: expect.any(DynamicValue),
			originalAttribute: "ng-class",
		}),
	]);
});

it("should strip templating from html", async () => {
	expect.assertions(2);
	const result = await transformString(angularTransformHTML, "<p>{{ foobar }}</p>");
	expect(result).toHaveLength(1);
	expect(result[0]).toMatchInlineSnapshot(`
		{
		  "column": 1,
		  "data": "<p>{{        }}</p>",
		  "filename": "inline",
		  "hooks": {
		    "processAttribute": [Function],
		    "processElement": [Function],
		  },
		  "line": 1,
		  "offset": 0,
		  "originalData": "<p>{{ foobar }}</p>",
		}
	`);
});
