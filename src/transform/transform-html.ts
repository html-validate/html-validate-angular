import { type Source } from "html-validate";
import { transform } from "./transform";

export function angularTransformHTML(source: Source): Source[] {
	return [transform(source)];
}

angularTransformHTML.api = 1;
