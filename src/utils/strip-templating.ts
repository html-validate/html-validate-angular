/**
 * Strip anything inside `{{ .. }}` in case it contains anything that looks like
 * markup, i.e. it will be ignored
 */
export function stripTemplating(str: string): string {
	/* eslint-disable-next-line sonarjs/slow-regex -- technical debt, should refactor */
	return str.replace(/{{(.*?)}}/g, (_, m: string) => {
		const filler = " ".repeat(m.length);
		return `{{${filler}}}`;
	});
}
