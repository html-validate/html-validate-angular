import { DynamicValue, AttributeData } from "html-validate";
import { processAttribute as processAttributeGenerator } from "./attribute";

function processAttribute(attr: AttributeData): AttributeData[] {
	return Array.from(processAttributeGenerator(attr));
}

it("should flag attributes with interpolation as dynamic", () => {
	expect.assertions(1);
	const attrs = processAttribute({
		key: "foo",
		value: "foo{{ bar }}baz",
		quote: "'",
	});
	expect(attrs).toEqual([
		{
			key: "foo",
			value: expect.any(DynamicValue),
			quote: "'",
		},
	]);
});

it("should handle ng-attr- as target attribute", () => {
	expect.assertions(1);
	const attrs = processAttribute({
		key: "ng-attr-foo",
		value: "bar",
		quote: "'",
	});
	expect(attrs).toEqual([
		{
			key: "ng-attr-foo",
			value: "bar",
			quote: "'",
		},
		{
			key: "foo",
			value: expect.any(DynamicValue),
			quote: "'",
			originalAttribute: "ng-attr-foo",
		},
	]);
});

it("should handle [foo] as property binding", () => {
	expect.assertions(1);
	const attrs = processAttribute({
		key: "[foo]",
		value: "bar",
		quote: "'",
	});
	expect(attrs).toEqual([
		{
			key: "[foo]",
			value: "bar",
			quote: "'",
		},
		{
			key: "foo",
			value: expect.any(DynamicValue),
			quote: "'",
			originalAttribute: "[foo]",
		},
	]);
});

it("should handle [attr.foo] as attribute binding", () => {
	expect.assertions(1);
	const attrs = processAttribute({
		key: "[attr.foo]",
		value: "bar",
		quote: "'",
	});
	expect(attrs).toEqual([
		{
			key: "[attr.foo]",
			value: "bar",
			quote: "'",
		},
		{
			key: "foo",
			value: expect.any(DynamicValue),
			quote: "'",
			originalAttribute: "[attr.foo]",
		},
	]);
});

it("should handle ng-required and friends as target attribute", () => {
	expect.assertions(1);
	const attrs = processAttribute({
		key: "ng-required",
		value: "foo",
		quote: "'",
	});
	expect(attrs).toEqual([
		{
			key: "ng-required",
			value: "foo",
			quote: "'",
		},
		{
			key: "required",
			value: expect.any(DynamicValue),
			quote: "'",
			originalAttribute: "ng-required",
		},
	]);
});

it("should handle ng-src with interpolation", () => {
	expect.assertions(1);
	const attrs = processAttribute({
		key: "ng-src",
		value: "{{ foo }}",
		quote: "'",
	});
	expect(attrs).toEqual([
		{
			key: "ng-src",
			value: "{{ foo }}",
			quote: "'",
		},
		{
			key: "src",
			value: expect.any(DynamicValue),
			quote: "'",
			originalAttribute: "ng-src",
		},
	]);
});

it("should leave other attributes intact", () => {
	expect.assertions(1);
	const attrs = processAttribute({
		key: "foo",
		value: "bar",
		quote: "'",
	});
	expect(attrs).toEqual([
		{
			key: "foo",
			value: "bar",
			quote: "'",
		},
	]);
});

it("should handle boolean attributes", () => {
	expect.assertions(1);
	const attrs = processAttribute({
		key: "foo",
		value: null,
		quote: "'",
	});
	expect(attrs).toEqual([
		{
			key: "foo",
			value: null,
			quote: "'",
		},
	]);
});
