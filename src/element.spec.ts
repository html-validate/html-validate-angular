import { Config, Parser, TextNode } from "html-validate";
import { processElement } from "./element";

let parser: Parser;

beforeAll(async () => {
	const config = Config.empty();
	const resolved = await config.resolve();
	parser = new Parser(resolved);
});

it("should add dynamic text when ng-bind is present", () => {
	expect.assertions(1);
	const markup = /* HTML */ ` <any ng-bind="value"></any> `;
	const document = parser.parseHtml(markup);
	const node = document.querySelector("any")!;
	processElement(node);
	expect(node.childNodes).toEqual([expect.any(TextNode)]);
});

it("should add dynamic text when ng-bind-html is present", () => {
	expect.assertions(1);
	const markup = /* HTML */ ` <any ng-bind-html="value"></any> `;
	const document = parser.parseHtml(markup);
	const node = document.querySelector("any")!;
	processElement(node);
	expect(node.childNodes).toEqual([expect.any(TextNode)]);
});

it("should add dynamic text when ng-bind-template is present", () => {
	expect.assertions(1);
	const markup = /* HTML */ ` <any ng-bind-template="value"></any> `;
	const document = parser.parseHtml(markup);
	const node = document.querySelector("any")!;
	processElement(node);
	expect(node.childNodes).toEqual([expect.any(TextNode)]);
});

it("should add dynamic text when translate is present", () => {
	expect.assertions(1);
	const markup = /* HTML */ ` <any translate="key"></any> `;
	const document = parser.parseHtml(markup);
	const node = document.querySelector("any")!;
	processElement(node);
	expect(node.childNodes).toEqual([expect.any(TextNode)]);
});

it("should add dynamic text when ng-transclude is present", () => {
	expect.assertions(1);
	const markup = /* HTML */ ` <any ng-transclude="value"></any> `;
	const document = parser.parseHtml(markup);
	const node = document.querySelector("any")!;
	processElement(node);
	expect(node.childNodes).toEqual([expect.any(TextNode)]);
});

it("should add dynamic text when node is <ng-transclude>", () => {
	expect.assertions(1);
	const markup = /* HTML */ ` <ng-transclude></ng-transclude> `;
	const document = parser.parseHtml(markup);
	const node = document.querySelector("ng-transclude")!;
	processElement(node);
	expect(node.childNodes).toEqual([expect.any(TextNode)]);
});

it("should add dynamic text when ng-pluralize is present", () => {
	expect.assertions(1);
	const markup = /* HTML */ ` <any ng-pluralize="value"></any> `;
	const document = parser.parseHtml(markup);
	const node = document.querySelector("any")!;
	processElement(node);
	expect(node.childNodes).toEqual([expect.any(TextNode)]);
});

it("should add dynamic text when node is <ng-pluralize>", () => {
	expect.assertions(1);
	const markup = /* HTML */ ` <ng-pluralize></ng-pluralize> `;
	const document = parser.parseHtml(markup);
	const node = document.querySelector("ng-pluralize")!;
	processElement(node);
	expect(node.childNodes).toEqual([expect.any(TextNode)]);
});

it("should not add dynamic text if no dynamic content attribute is present", () => {
	expect.assertions(1);
	const markup = /* HTML */ ` <any></any> `;
	const document = parser.parseHtml(markup);
	const node = document.querySelector("any")!;
	processElement(node);
	expect(node.childNodes).toEqual([]);
});
