import fs from "node:fs/promises";
import { build as esbuild, analyzeMetafile } from "esbuild";

const entrypoint = {
	cjs: "src/entry-cjs.ts",
	esm: "src/entry-esm.ts",
};

const extension = {
	cjs: ".cjs",
	esm: ".mjs",
};

await fs.rm("dist", { recursive: true, force: true });

for (const format of ["cjs", "esm"]) {
	const result = await esbuild({
		entryPoints: [{ in: entrypoint[format], out: "index" }],
		outdir: `dist/${format}`,
		bundle: true,
		platform: "node",
		format,
		target: "node18",
		logLevel: "info",
		sourcemap: true,
		metafile: true,
		outExtension: {
			".js": extension[format],
		},
		external: ["@html-validate/plugin-utils", "html-validate"],
	});
	if (format === "esm") {
		/* eslint-disable-next-line no-console -- expected to log */
		console.log(await analyzeMetafile(result.metafile));
	}
}
